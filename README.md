This plugin illustrates a problem using spring beans inside of a ResourceFilter. The short version is that I haven't found a good, cross-product, declarative way to inject my plugin's services inside of a ResourceFilter.

I've created this simple Jira plugin to illustrate the problem.

You can see that while the plugin's service, MyPluginComponent and its implementation get exported just fine, and can even be injected into a resource like MyFrigginResource (see commit 7ce5a1fa8f85ef7f054b2dfc351212eb6d524e7a), the filter that relies on it fails to initialize. Visit http://localhost:2990/jira/rest/frick/latest/me to reproduce the following error:

```
2018-10-26 14:58:45,963 http-nio-2990-exec-8 ERROR admin 898x272x1 ios084 0:0:0:0:0:0:0:1 /rest/frick/latest/me [c.a.plugin.servlet.DefaultServletModuleManager] Unable to create new reference LazyLoadedFilterReference{descriptor=com.adaptavist.jonny.filter-plugin-with-spring-scanner:my-rest-filter (null), filterConfig=com.atlassian.plugin.servlet.filter.PluginFilterConfig@26fb3d52}
com.atlassian.util.concurrent.LazyReference$InitializationException: com.sun.jersey.spi.inject.Errors$ErrorMessagesException
	at com.atlassian.util.concurrent.LazyReference.getInterruptibly(LazyReference.java:149)
	at com.atlassian.util.concurrent.LazyReference.get(LazyReference.java:112)
	at com.atlassian.plugin.servlet.DefaultServletModuleManager.getInstance(DefaultServletModuleManager.java:447)
	at com.atlassian.plugin.servlet.DefaultServletModuleManager.getFilter(DefaultServletModuleManager.java:440)
	at com.atlassian.plugin.servlet.DefaultServletModuleManager.getFilters(DefaultServletModuleManager.java:300)
	at com.atlassian.plugins.rest.module.servlet.DefaultRestServletModuleManager.getFilters(DefaultRestServletModuleManager.java:121)
	... 39 filtered
	at com.atlassian.web.servlet.plugin.request.RedirectInterceptingFilter.doFilter(RedirectInterceptingFilter.java:21)
	... 24 filtered
	at com.atlassian.labs.httpservice.resource.ResourceFilter.doFilter(ResourceFilter.java:59)
	... 32 filtered
	at com.atlassian.jira.security.JiraSecurityFilter.lambda$doFilter$0(JiraSecurityFilter.java:66)
	... 1 filtered
	at com.atlassian.jira.security.JiraSecurityFilter.doFilter(JiraSecurityFilter.java:64)
	... 16 filtered
	at com.atlassian.plugins.rest.module.servlet.RestSeraphFilter.doFilter(RestSeraphFilter.java:37)
	... 19 filtered
	at com.atlassian.jira.servermetrics.CorrelationIdPopulatorFilter.doFilter(CorrelationIdPopulatorFilter.java:30)
	... 10 filtered
	at com.atlassian.web.servlet.plugin.request.RedirectInterceptingFilter.doFilter(RedirectInterceptingFilter.java:21)
	... 4 filtered
	at com.atlassian.web.servlet.plugin.LocationCleanerFilter.doFilter(LocationCleanerFilter.java:36)
	... 26 filtered
	at com.atlassian.jira.servermetrics.MetricsCollectorFilter.doFilter(MetricsCollectorFilter.java:25)
	... 23 filtered
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
	at java.lang.Thread.run(Thread.java:748)
Caused by: com.sun.jersey.spi.inject.Errors$ErrorMessagesException
	at com.sun.jersey.spi.inject.Errors.processErrorMessages(Errors.java:170)
	at com.sun.jersey.spi.inject.Errors.postProcess(Errors.java:136)
	at com.sun.jersey.spi.inject.Errors.processWithErrors(Errors.java:199)
	at com.sun.jersey.server.impl.application.WebApplicationImpl.initiate(WebApplicationImpl.java:795)
	at com.atlassian.plugins.rest.module.RestDelegatingServletFilter$JerseyOsgiServletContainer.initiate(RestDelegatingServletFilter.java:159)
	... 4 filtered
	at com.atlassian.plugins.rest.module.RestDelegatingServletFilter.initServletContainer(RestDelegatingServletFilter.java:88)
	at com.atlassian.plugins.rest.module.RestDelegatingServletFilter.init(RestDelegatingServletFilter.java:61)
	... 1 filtered
	at com.atlassian.plugin.servlet.DefaultServletModuleManager$LazyLoadedFilterReference.create(DefaultServletModuleManager.java:516)
	at com.atlassian.plugin.servlet.DefaultServletModuleManager$LazyLoadedFilterReference.create(DefaultServletModuleManager.java:503)
	at com.atlassian.util.concurrent.LazyReference$Sync.run(LazyReference.java:325)
	at com.atlassian.util.concurrent.LazyReference.getInterruptibly(LazyReference.java:143)
	... 212 more
```

If I debug the error at the LazyReference.getInterruptibly method, I can see that the underlying `com.sun.jersy.spi.inject.Error$ErrorMessagesException` contains the following message:

> Missing dependency for constructor public com.adaptavist.jonny.filters.MyResourceFilter(com.adaptavist.jonny.api.MyPluginComponent) at parameter index 0

In some conditions, it will also have:

> The class com.adaptavist.jonny.api.MyPluginComponent is an interface and cannot be instantiated.

I've tried a few workarounds.

* The first (and most obvious) thing I tried was simply using good ol' @Inject on the filter's constructor (see 89fcea1ad781d0ade3fece5f5dcaa6e7d223832d). Notably, this fails even if I annotate the filter with `@Named` and `@ExportAsService`. Even if the filter gets wired up as a bean, the version that Jira's servlet manager invokes doesn't have its dependencies wired in.
* [The received wisdom for plain Jersey seems to be to use InjectParam](https://stackoverflow.com/questions/37319909/are-injectparam-and-autowired-same). This fails with the same error (see commit 45235f8397a9b74b561b51ad232a2f080e828de1).
* I can call `com.atlassian.jira.component.ComponentAccessor.getOSGiComponentInstanceOfType(MyPluginComponent.class)` in the constructor (see commit 9fdf2e57017b6c09957806a374a448b5e131d9db and ae005214af55fa2007656a80f7a901dae04b5e89). That works (I notice Atlassian does something similar inside Jira in AbstractInstrumentResourceFilter's init method), but I dislike that approach for a couple of reasons:
    1. ComponentAccessor only exists in Jira; this approach won't work for other apps.
    1. It should be possible to do regular dependency injection of my own custom components in a ResourceFilter.
* Implementing a custom ResourceFilterFactory, and exporting that as a service (see MyResourceFilterFactory.java). While this example just filters all requests to any Resource in my plugin, I could modify it to look for the presence of a custom annotation, or some similar means of discriminating between endpoints. This seems to be the pattern followed by the WebSudoRequired annotation. Credit to Gabriel Morales for his comment on https://community.atlassian.com/t5/Answers-Developer-Questions/How-can-I-add-a-custom-filter-for-my-rest-resources-requests/qaq-p/554620 that prompted me to try this.

I'm pretty sure the reason that `@InjectParam` and `@Inject` don't work is the way that 
`com.atlassian.plugin.servlet.DefaultServletModuleManager` and
`com.atlassian.plugin.servlet.filter.DelegatingPluginFilter` conspire to wrap any filters. I think this bypasses any
spring or jersey conventions that would properly inject beans. I haven't proven this to myself to perfect certainty,
and there may be a way to insinuate dependency injection back into this process, but I've not yet found it.

So far, the best way to get spring beans into a filter seems to be injecting them via a ResourceFilterFactory. I'd love 
to hear approaches from others, though.
