package com.adaptavist.jonny.filters;

import com.adaptavist.jonny.api.MyPluginComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.ext.Provider;
import java.util.Collections;
import java.util.List;

@ExportAsService
@Named
@Provider
public class MyResourceFilterFactory implements ResourceFilterFactory {

    public MyPluginComponent myPluginComponent;

    @Inject
    MyResourceFilterFactory(MyPluginComponent myPluginComponent) {
        this.myPluginComponent = myPluginComponent;
    }

    @Override
    public List<ResourceFilter> create(AbstractMethod am) {
        return Collections.singletonList( new MyResourceFilter(myPluginComponent) );
    }
}
