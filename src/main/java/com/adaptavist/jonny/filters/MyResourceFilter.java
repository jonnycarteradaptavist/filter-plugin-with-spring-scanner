package com.adaptavist.jonny.filters;

import com.adaptavist.jonny.api.MyPluginComponent;
import com.atlassian.plugins.rest.common.security.AuthorisationException;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;

import javax.ws.rs.ext.Provider;

@Provider
public class MyResourceFilter implements ResourceFilter, ContainerRequestFilter {

    private MyPluginComponent myPluginComponent;

    public MyResourceFilter( MyPluginComponent myPluginComponent ) {
        this.myPluginComponent = myPluginComponent;
    }

    @Override
    public ContainerRequest filter(ContainerRequest request) {
        if (myPluginComponent.getName().contains("myComponent")) {
            throw new AuthorisationException("I ain't gonna let you do jack diddly, bub.");
        }
        return request;
    }

    @Override
    public ContainerRequestFilter getRequestFilter() {
        return this;
    }

    @Override
    public ContainerResponseFilter getResponseFilter() {
        return null;
    }
}
