package com.adaptavist.jonny.rest;

import com.adaptavist.jonny.api.MyPluginComponent;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("me")
public class MyFrigginResource {

    MyPluginComponent myPluginComponent;

    @Inject
    MyFrigginResource(MyPluginComponent myPluginComponent) {
        this.myPluginComponent = myPluginComponent;
    }

    @GET
    public Response doSomething() {
        final String name = myPluginComponent.getName();
        System.out.println("The plugin name is " + name);
        return Response.ok(name).build();
    }
}
