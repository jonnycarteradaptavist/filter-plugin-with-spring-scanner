package com.adaptavist.jonny.api;

public interface MyPluginComponent
{
    String getName();
}